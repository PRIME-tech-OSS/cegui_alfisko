# CEGUI Alfisko theme

Main developer: [Huy Nguyen](https://github.com/Florastamine/)

![](screenshot.png)

Main difference(s) from the original Alfisko:
* `ColourPicker`, `Listbox`, `Combobox`, `ComboDropList`, `DropDownMenu`, `Tooltip` & `Spinner` widgets, ported from the vanilla theme & modified to match the Alfisko look.
* Blue & red color scheme.
